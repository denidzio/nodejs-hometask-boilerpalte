function makeLog(fighterOne, fighterTwo) {
  return {
    fighter1Shot: fighterOne.shot,
    fighter2Shot: fighterTwo.shot,
    fighter1Health: fighterOne.health,
    fighter2Health: fighterTwo.health,
  };
}

const logger = {
  fighterOneIsAttacker(fighterOne, fighterTwo) {
    return makeLog(
      {
        shot: fighterTwo.healthBeforeAttack - fighterTwo.health,
        health: fighterOne.health,
      },
      { shot: 0, health: fighterTwo.health },
    );
  },
  fighterTwoIsAttacker(fighterOne, fighterTwo) {
    return makeLog(
      {
        shot: 0,
        health: fighterOne.health,
      },
      { shot: fighterOne.healthBeforeAttack - fighterOne.health, health: fighterTwo.health },
    );
  },
};

export default logger;
