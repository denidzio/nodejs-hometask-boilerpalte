import { getDamage, getCritDamage } from '../services/fightService';

function attackAction(defender, hitPower) {
  const defenderHealth = defender.health > hitPower ? defender.health - hitPower : 0;
  return { ...defender, health: defenderHealth };
}

export function blockEnable(fighter) {
  return { ...fighter, inBlock: true };
}

export function blockDisable(fighter) {
  return { ...fighter, inBlock: false };
}

export function critAttack(attacker, defender) {
  if (attacker.inBlock) {
    return { attacker, defender };
  }

  const now = Date.now();

  if (now - attacker.lastCritHit < 10 * 1000) {
    return { attacker, defender };
  }

  const defenderAfterAttack = attackAction(defender, getCritDamage(attacker));

  return { attacker: { ...attacker, lastCritHit: now }, defender: defenderAfterAttack };
}

export function simpleAttack(attacker, defender) {
  if (attacker.inBlock || defender.inBlock) {
    return defender;
  }

  const defenderAfterAttack = attackAction(defender, getDamage(attacker, defender));

  return defenderAfterAttack;
}
