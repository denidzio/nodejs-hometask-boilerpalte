export function getHealthBarWidth(fighter) {
  const indicatorWidth = fighter.health > 0 ? (fighter.health * 100) / fighter.fullHealth : 0;
  return `${indicatorWidth}%`;
}

export function getDamage(attacker, defender) {
  // return damage
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);

  const damage = hitPower - blockPower;
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  return fighter.power * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense * (Math.random() + 1);
}

export function getCritDamage(fighter) {
  return 2 * fighter.power;
}
