import React, { useState } from 'react';

import { createFight } from '../../services/domainRequest/fightRequest';
import Game from '../game';
import WinnerModal from '../winnerModal';
import ArenaFighter from '../arenaFighter/';
import HealthIndicator from '../healthIndicator';
import { getHealthBarWidth } from '../../services/fightService';

import arenaBg from '../../resources/arena.jpg';
import './arena.css';

let logs = [];

function setLogs(settedLogs) {
  logs = settedLogs;
}

function Arena({ fighter1, fighter2 }) {
  const [gameResult, setGameResult] = useState({ isEnd: false, winner: null });

  const [fighterOne, setFighterOne] = useState({
    ...fighter1,
    fullHealth: fighter1.health,
    inBlock: false,
    lastCritHit: 0,
  });

  const [fighterTwo, setFighterTwo] = useState({
    ...fighter2,
    fullHealth: fighter2.health,
    inBlock: false,
    lastCritHit: 0,
  });

  const handleGameResult = (attacker, defender) => {
    if (defender.health === 0) {
      createFight({ fighter1: fighter1.id, fighter2: fighter2.id, log: logs });
      setGameResult({ isEnd: true, winner: attacker });
    }
  };

  return (
    <Game
      backgroundImage={arenaBg}
      className="arena___root"
      fighterOne={fighterOne}
      fighterTwo={fighterTwo}
      gameResult={gameResult}
      onSetFighterOne={setFighterOne}
      onSetFighterTwo={setFighterTwo}
      onSetGameResult={handleGameResult}
      onSetLogs={setLogs}>
      <div className="arena___fight-status">
        <HealthIndicator width={getHealthBarWidth(fighterOne)} title={fighter1.name} />
        <div className="arena___versus-sign"></div>
        <HealthIndicator width={getHealthBarWidth(fighterTwo)} title={fighter2.name} />
      </div>
      <div className="arena___battlefield">
        <ArenaFighter className="arena___left-fighter" fighter={fighter1} />
        <ArenaFighter className="arena___right-fighter" fighter={fighter2} />
      </div>
      <WinnerModal open={gameResult.isEnd} winner={gameResult.winner && gameResult.winner.name} />
    </Game>
  );
}
export default Arena;
