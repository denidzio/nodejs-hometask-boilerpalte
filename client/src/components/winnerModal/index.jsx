import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Modal, Button } from '@material-ui/core/';

import './winnerModal.css';

const useStyles = makeStyles((theme) => ({
  paper: {
    width: 250,
    padding: theme.spacing(2, 3, 3),

    borderRadius: '2px',
    border: `4px solid ${theme.palette.warning.dark}`,
    backgroundColor: theme.palette.warning.light,
    boxShadow: theme.shadows[5],
  },
}));

function WinnerModal({ open, winner }) {
  const classes = useStyles();

  const handleClickStartAgain = () => {
    window.location.reload();
  };

  return (
    <Modal open={open}>
      <div id="winner-modal" className={classes.paper}>
        <h2 id="winner-modal-title">The end!</h2>
        <p id="winner-modal-description">
          The winner is <b>{winner}</b>
        </p>
        <Button variant="contained" onClick={handleClickStartAgain} className={classes.button}>
          Start again
        </Button>
      </div>
    </Modal>
  );
}

export default WinnerModal;
