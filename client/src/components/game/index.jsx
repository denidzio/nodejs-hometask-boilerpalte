import React from 'react';

import logger from '../../services/logHelper';
import { critAttack, simpleAttack, blockEnable, blockDisable } from '../../services/fightActions';
import { controls } from '../../constants/controls';
import { keyAction, multipleKeyAction } from '../../services/keyboardHelper';

const firstPlayerPressedKeys = new Set();
const secondPlayerPressedKeys = new Set();

let logs = [];

function addLog(log) {
  logs = [...logs, log];
}

function Game({
  children,
  backgroundImage,
  className,
  fighterOne,
  fighterTwo,
  gameResult,
  onSetFighterOne,
  onSetFighterTwo,
  onSetGameResult,
  onSetLogs,
}) {
  const handleGameResult = (attacker, defender) => {
    onSetLogs(logs);
    onSetGameResult(attacker, defender);
  };

  const handleKeyDown = (event) => {
    if (gameResult.isEnd) {
      return;
    }

    keyAction(event, controls.PlayerOneBlock, () => {
      const playerOneBlockEnable = blockEnable(fighterOne);
      onSetFighterOne(playerOneBlockEnable);
    });

    keyAction(event, controls.PlayerTwoBlock, () => {
      const playerTwoBlockEnable = blockEnable(fighterTwo);
      onSetFighterTwo(playerTwoBlockEnable);
    });

    multipleKeyAction(event, controls.PlayerOneCriticalHitCombination, () => {
      firstPlayerPressedKeys.add(event.keyCode);

      if (firstPlayerPressedKeys.size !== controls.PlayerOneCriticalHitCombination.length) {
        return;
      }

      const fightersAfterAttack = critAttack(fighterOne, fighterTwo);

      addLog(
        logger.fighterOneIsAttacker(fighterOne, {
          health: fightersAfterAttack.defender.health,
          healthBeforeAttack: fighterTwo.health,
        }),
      );

      onSetFighterOne(fightersAfterAttack.attacker);
      onSetFighterTwo(fightersAfterAttack.defender);

      firstPlayerPressedKeys.clear();

      handleGameResult(fighterOne, fightersAfterAttack.defender);
    });

    multipleKeyAction(event, controls.PlayerTwoCriticalHitCombination, () => {
      secondPlayerPressedKeys.add(event.keyCode);

      if (secondPlayerPressedKeys.size !== controls.PlayerTwoCriticalHitCombination.length) {
        return;
      }

      const fightersAfterAttack = critAttack(fighterTwo, fighterOne);

      addLog(
        logger.fighterTwoIsAttacker(
          {
            health: fightersAfterAttack.defender.health,
            healthBeforeAttack: fighterOne.health,
          },
          fighterTwo,
        ),
      );

      onSetFighterOne(fightersAfterAttack.defender);
      onSetFighterTwo(fightersAfterAttack.attacker);

      secondPlayerPressedKeys.clear();

      handleGameResult(fighterTwo, fightersAfterAttack.defender);
    });
  };

  const handleKeyUp = (event) => {
    if (gameResult.isEnd) {
      return;
    }

    keyAction(event, controls.PlayerOneAttack, () => {
      const fighterTwoAfterAttack = simpleAttack(fighterOne, fighterTwo);

      addLog(
        logger.fighterOneIsAttacker(fighterOne, {
          health: fighterTwoAfterAttack.health,
          healthBeforeAttack: fighterTwo.health,
        }),
      );

      onSetFighterTwo(fighterTwoAfterAttack);
      handleGameResult(fighterOne, fighterTwoAfterAttack);
    });

    keyAction(event, controls.PlayerTwoAttack, () => {
      const fighterOneAfterAttack = simpleAttack(fighterTwo, fighterOne);

      addLog(
        logger.fighterTwoIsAttacker(
          {
            health: fighterOneAfterAttack.health,
            healthBeforeAttack: fighterOne.health,
          },
          fighterTwo,
        ),
      );

      onSetFighterOne(fighterOneAfterAttack);
      handleGameResult(fighterTwo, fighterOneAfterAttack);
    });

    keyAction(event, controls.PlayerOneBlock, () => {
      const playerOneBlockDisable = blockDisable(fighterOne);
      onSetFighterOne(playerOneBlockDisable);
    });

    keyAction(event, controls.PlayerTwoBlock, () => {
      const playerTwoBlockDisable = blockDisable(fighterTwo);
      onSetFighterTwo(playerTwoBlockDisable);
    });

    multipleKeyAction(event, controls.PlayerOneCriticalHitCombination, () => {
      firstPlayerPressedKeys.delete(event.keyCode);
    });

    multipleKeyAction(event, controls.PlayerTwoCriticalHitCombination, () => {
      secondPlayerPressedKeys.delete(event.keyCode);
    });
  };

  return (
    <div
      style={{ backgroundImage: `url(${backgroundImage})` }}
      className={className}
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
      tabIndex="0">
      {children}
    </div>
  );
}

export default Game;
