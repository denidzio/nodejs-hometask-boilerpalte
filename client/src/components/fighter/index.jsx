import React, { useState } from 'react';
import { FormControl, InputLabel, makeStyles, Select } from '@material-ui/core';
import { MenuItem } from 'material-ui';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    maxWidth: 200,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  selectedFighter: {
    marginTop: theme.spacing(1),
    border: '1px solid rgba(0, 0, 0, 0.1)',
    padding: theme.spacing(1),
  },
}));

export default function Fighter({ fightersList, onFighterSelect, selectedFighter }) {
  const classes = useStyles();

  const [fighter, setFighter] = useState(fightersList.length ? fightersList[0] : '');

  const handleChange = (event) => {
    // debugger;
    setFighter(event.target.value);
    onFighterSelect(event.target.value);
  };

  return (
    <FormControl className={classes.formControl}>
      <InputLabel id="simple-select-label">Select Fighter</InputLabel>
      <Select
        labelId="simple-select-label"
        id="simple-select"
        value={fighter}
        onChange={handleChange}>
        {fightersList.map((it, index) => {
          return (
            <MenuItem key={`${index}`} value={it}>
              {it.name}
            </MenuItem>
          );
        })}
      </Select>
      {selectedFighter ? (
        <div className={classes.selectedFighter}>
          <div>Name: {selectedFighter.name}</div>
          <div>Power: {selectedFighter.power}</div>
          <div>Defense: {selectedFighter.defense}</div>
          <div>Health: {selectedFighter.health}</div>
        </div>
      ) : null}
    </FormControl>
  );
}
