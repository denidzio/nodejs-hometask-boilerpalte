import React, { useEffect, useState } from 'react';
import {
  Table,
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
} from '@material-ui/core';

import { getFighter } from '../../services/domainRequest/fightersRequest';
import { getFights, deleteFight } from '../../services/domainRequest/fightRequest';
import './fightHistory.css';

async function getExtendedFights() {
  const fights = await getFights();

  const extendedFights = [];

  for (const fight of fights) {
    const fighter1 = (await getFighter(fight.fighter1)).name;
    const fighter2 = (await getFighter(fight.fighter2)).name;

    const winner = fight.log.find((log) => log.fighter1Health === 0 || log.fighter2Health === 0);

    extendedFights.push({
      id: fight.id,
      winner: winner && winner.fighter1Health === 0 ? fighter2 : fighter1,
      fighter1,
      fighter2,
    });
  }

  return extendedFights;
}

function FightHistory() {
  const [deleteItem, setDeletedItem] = useState(null);
  const [fights, setFights] = useState([]);

  const handleDeleteItem = async (id) => {
    setDeletedItem(await deleteFight(id));
  };

  useEffect(() => {
    getExtendedFights().then((fights) => setFights(fights));
  }, [deleteItem]);

  return (
    <div id="history">
      <div>History</div>
      {fights.length ? (
        <div>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell align="center">#</TableCell>
                  <TableCell align="center">Fighter 1 Name</TableCell>
                  <TableCell align="center">Fighter 2 Name</TableCell>
                  <TableCell align="center">Winner</TableCell>
                  <TableCell align="center">Delete</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {fights
                  .map((fight, index) => (
                    <TableRow key={fight.id}>
                      <TableCell align="center">{index + 1}</TableCell>
                      <TableCell align="center">{fight.fighter1}</TableCell>
                      <TableCell align="center">{fight.fighter2}</TableCell>
                      <TableCell align="center">{fight.winner}</TableCell>
                      <TableCell
                        align="center"
                        className="delete-history-item"
                        onClick={() => handleDeleteItem(fight.id)}>
                        ×
                      </TableCell>
                    </TableRow>
                  ))
                  .reverse()}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      ) : (
        <div>No fights yet...</div>
      )}
    </div>
  );
}

export default FightHistory;
