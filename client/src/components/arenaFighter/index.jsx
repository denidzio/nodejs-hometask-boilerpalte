import React from 'react';

import '../arenaFighter/arenaFighter.css';

function ArenaFighter({ fighter, className }) {
  return (
    <div className={`arena-fighter ${className}`}>
      <img
        className="arena-fighter__preview"
        src="https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif"
        title={fighter.name}
        alt={fighter.name}
      />
    </div>
  );
}

export default ArenaFighter;
