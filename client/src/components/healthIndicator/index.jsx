import React from 'react';

function HealthIndicator({ width, title }) {
  return (
    <div className="arena___fighter-indicator">
      <span className="arena___fighter-name">{title}</span>
      <div className="arena___health-indicator">
        <div
          className="arena___health-bar"
          id="left-fighter-indicator"
          style={{ width: width }}></div>
      </div>
    </div>
  );
}

export default HealthIndicator;
