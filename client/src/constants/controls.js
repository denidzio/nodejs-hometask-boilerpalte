export const controls = {
  PlayerOneAttack: 65,
  PlayerOneBlock: 68,
  PlayerTwoAttack: 74,
  PlayerTwoBlock: 76,
  PlayerOneCriticalHitCombination: [81, 87, 69],
  PlayerTwoCriticalHitCombination: [85, 73, 79],
};
