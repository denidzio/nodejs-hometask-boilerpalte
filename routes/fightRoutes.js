const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
  createFightValid,
  updateFightValid,
} = require('../middlewares/fight.validation.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    const result = FightService.getAll();

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 404, message: 'Fights not found' };
    }

    next();
  },
  responseMiddleware,
);

router.get(
  '/:id',
  (req, res, next) => {
    const result = FightService.getOne(req.params.id);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 404, message: 'Fight not found' };
    }

    next();
  },
  responseMiddleware,
);

router.post(
  '/',
  createFightValid,
  (req, res, next) => {
    if (res.locals.error) {
      next();
      return;
    }

    const result = FightService.create(req.body);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 400, message: 'Failed to create fight' };
    }

    next();
  },
  responseMiddleware,
);

router.put(
  '/:id',
  updateFightValid,
  (req, res, next) => {
    if (res.locals.error) {
      next();
      return;
    }

    const result = FightService.update(req.params.id, req.body);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 400, message: 'Failed to update fight' };
    }

    next();
  },
  responseMiddleware,
);

router.delete(
  '/:id',
  (req, res, next) => {
    const result = FightService.delete(req.params.id);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 404, message: 'Fight not found' };
    }

    next();
  },
  responseMiddleware,
);

module.exports = router;
