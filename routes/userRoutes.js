const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    const result = UserService.getAll();

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 404, message: 'Users not found' };
    }

    next();
  },
  responseMiddleware,
);

router.get(
  '/:id',
  (req, res, next) => {
    const result = UserService.getOne(req.params.id);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 404, message: 'User not found' };
    }

    next();
  },
  responseMiddleware,
);

router.post(
  '/',
  createUserValid,
  (req, res, next) => {
    if (res.locals.error) {
      next();
      return;
    }

    const result = UserService.create(req.body);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 400, message: 'Failed to create user' };
    }

    next();
  },
  responseMiddleware,
);

router.put(
  '/:id',
  updateUserValid,
  (req, res, next) => {
    if (res.locals.error) {
      next();
      return;
    }

    const result = UserService.update(req.params.id, req.body);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 400, message: 'Failed to update user' };
    }

    next();
  },
  responseMiddleware,
);

router.delete(
  '/:id',
  (req, res, next) => {
    const result = UserService.delete(req.params.id);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 404, message: 'User not found' };
    }

    next();
  },
  responseMiddleware,
);

module.exports = router;
