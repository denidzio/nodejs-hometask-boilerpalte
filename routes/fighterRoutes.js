const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
  createFighterValid,
  updateFighterValid,
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    const result = FighterService.getAll();

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 404, message: 'Fighters not found' };
    }

    next();
  },
  responseMiddleware,
);

router.get(
  '/:id',
  (req, res, next) => {
    const result = FighterService.getOne(req.params.id);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 404, message: 'Fighter not found' };
    }

    next();
  },
  responseMiddleware,
);

router.post(
  '/',
  createFighterValid,
  (req, res, next) => {
    if (res.locals.error) {
      next();
      return;
    }

    const result = FighterService.create(req.body);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 400, message: 'Failed to create fighter' };
    }

    next();
  },
  responseMiddleware,
);

router.put(
  '/:id',
  updateFighterValid,
  (req, res, next) => {
    if (res.locals.error) {
      next();
      return;
    }

    const result = FighterService.update(req.params.id, req.body);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 400, message: 'Failed to update fighter' };
    }

    next();
  },
  responseMiddleware,
);

router.delete(
  '/:id',
  (req, res, next) => {
    const result = FighterService.delete(req.params.id);

    if (result) {
      res.locals.data = result;
    } else {
      res.locals.error = { code: 404, message: 'Fighter not found' };
    }

    next();
  },
  responseMiddleware,
);

module.exports = router;
