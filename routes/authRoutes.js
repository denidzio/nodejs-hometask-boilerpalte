const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
  '/login',
  (req, res, next) => {
    try {
      const user = AuthService.login(req.body);
      res.locals.data = user;
    } catch (err) {
      res.locals.error = {
        code: 404,
        message: err.message,
      };
    } finally {
      next();
    }
  },
  responseMiddleware,
);

module.exports = router;
