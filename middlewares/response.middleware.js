const responseMiddleware = (req, res, next) => {
  if (res.locals.error) {
    res.status(res.locals.error.code).send({
      error: true,
      message: `Error ${res.locals.error.code} - ${res.locals.error.message}`,
    });
    return;
  }

  res.send(res.locals.data);
};

exports.responseMiddleware = responseMiddleware;
