const { fight } = require('../models/fight');
const { createValidation, updateValidation } = require('./common.validation.middleware');

const allowedFields = ['fighter1', 'fighter2', 'log'];

const basicValidation = (data) => {
  return data;
};

const createFightValid = (req, res, next) => {
  res.locals.error = { code: 400, message: "Fight entity to create isn't valid" };

  const validData = createValidation(req, allowedFields, basicValidation);

  if (!validData) {
    next();
    return;
  }

  delete res.locals.error;

  req.body = validData;
  next();
};

const updateFightValid = (req, res, next) => {
  res.locals.error = { code: 400, message: "Fight entity to update isn't valid" };

  const validData = updateValidation(req, allowedFields, basicValidation);

  if (!validData) {
    next();
    return;
  }

  delete res.locals.error;

  req.body = validData;
  next();
};

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;
