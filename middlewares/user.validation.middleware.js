const { user } = require('../models/user');
const {
  createValidation,
  updateValidation,
  isEmpty,
  isString,
} = require('./common.validation.middleware');

const allowedFields = ['firstName', 'lastName', 'email', 'phoneNumber', 'password'];

const basicValidation = (data) => {
  const { email, phoneNumber, password } = data;

  if (!isEmpty(email) && !email.match(/^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/i)) {
    return;
  }

  if (!isEmpty(phoneNumber) && !phoneNumber.match(/^\+380\d{9}$/)) {
    return;
  }

  if (!isEmpty(password) && (!isString(password) || password.length < 3)) {
    return;
  }

  return data;
};

const createUserValid = (req, res, next) => {
  res.locals.error = { code: 400, message: "User entity to create isn't valid" };

  const validData = createValidation(req, allowedFields, basicValidation);

  if (!validData) {
    next();
    return;
  }

  delete res.locals.error;

  req.body = validData;
  next();
};

const updateUserValid = (req, res, next) => {
  res.locals.error = { code: 400, message: "User entity to update isn't valid" };

  const validData = updateValidation(req, allowedFields, basicValidation);

  if (!validData) {
    next();
    return;
  }

  delete res.locals.error;

  req.body = validData;
  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
