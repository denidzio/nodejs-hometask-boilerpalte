const { fighter } = require('../models/fighter');
const {
  createValidation,
  updateValidation,
  isEmpty,
  isNumber,
} = require('./common.validation.middleware');

const allowedFields = ['name', 'health', 'power', 'defense'];

const basicValidation = (data) => {
  const { power, defense, health } = data;

  if (!isEmpty(power) && (!isNumber(power) || power < 1 || power > 100)) {
    return;
  }

  if (!isEmpty(defense) && (!isNumber(defense) || defense < 1 || defense > 10)) {
    return;
  }

  if (!isEmpty(health) && (!isNumber(health) || health < 80 || health > 120)) {
    return;
  }

  return data;
};

const createFighterValid = (req, res, next) => {
  res.locals.error = { code: 400, message: "Fighter entity to create isn't valid" };

  const validData = createValidation(req, allowedFields, basicValidation, [
    { key: 'health', defaultValue: fighter.health },
  ]);

  if (!validData) {
    next();
    return;
  }

  delete res.locals.error;

  req.body = validData;
  next();
};

const updateFighterValid = (req, res, next) => {
  res.locals.error = { code: 400, message: "Fighter entity to update isn't valid" };

  const validData = updateValidation(req, allowedFields, basicValidation);

  if (!validData) {
    next();
    return;
  }

  delete res.locals.error;

  req.body = validData;
  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
