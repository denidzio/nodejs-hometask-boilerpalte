const isThereExtraField = (keys, allowedFields) => {
  if (keys.some((key) => !allowedFields.includes(key))) {
    return true;
  }

  return false;
};

const isEmpty = (value) => {
  return value === undefined;
};

const isNumber = (value) => {
  return typeof value === 'number';
};

const isString = (value) => {
  return typeof value === 'string';
};

const createValidation = (req, allowedFields, validFunc, optionalFields = []) => {
  const data = req.body;
  const dataKeys = Object.keys(data);

  if (isThereExtraField(dataKeys, allowedFields)) {
    return;
  }

  if (
    !allowedFields.every(
      (field) =>
        dataKeys.includes(field) || optionalFields.some((opField) => opField.key === field),
    )
  ) {
    return;
  }

  const basicValidData = validFunc(data);

  if (!basicValidData) {
    return;
  }

  return {
    ...basicValidData,
    ...optionalFields.reduce(
      (acc, opField) =>
        (acc = {
          ...acc,
          [opField.key]: isEmpty(basicValidData[opField.key])
            ? opField.defaultValue
            : basicValidData[opField.key],
        }),
      {},
    ),
  };
};

const updateValidation = (req, allowedFields, validFunc) => {
  if (!req.params.id) {
    return;
  }

  const data = req.body;
  const dataKeys = Object.keys(data);

  if (!dataKeys.length) {
    return;
  }

  if (isThereExtraField(dataKeys, allowedFields)) {
    return;
  }

  const basicValidData = validFunc(data);

  if (!basicValidData) {
    return;
  }

  return { ...basicValidData };
};

exports.isEmpty = isEmpty;
exports.isNumber = isNumber;
exports.isString = isString;

exports.createValidation = createValidation;
exports.updateValidation = updateValidation;
