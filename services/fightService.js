const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
  search(search) {
    const item = FightRepository.getOne(search);

    if (!item) {
      return null;
    }

    return item;
  }

  getAll() {
    const items = FightRepository.getAll();

    if (!items) {
      return null;
    }

    return items;
  }

  getOne() {
    return this.search({ id });
  }

  create(data) {
    const fight = FightRepository.create(data);

    if (!fight) {
      return null;
    }

    return fight;
  }

  update(id, dataToUpdate) {
    if (!this.getOne(id)) {
      return null;
    }

    const updatedFight = FightRepository.update(id, dataToUpdate);

    if (!updatedFight) {
      return null;
    }

    return updatedFight;
  }

  delete(id) {
    const deletedFight = FightRepository.delete(id);

    if (!deletedFight.length) {
      return null;
    }

    return deletedFight;
  }
}

module.exports = new FightersService();
