const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  getSimilarFighter(data) {
    if (!data.name) {
      return;
    }

    return this.search({ name: data.name });
  }

  getInsensitiveData(data) {
    const insensitiveData = { ...data };

    if (data.name) {
      insensitiveData.name = data.name[0].toUpperCase() + data.name.slice(1).toLowerCase();
    }

    return insensitiveData;
  }

  search(search) {
    const item = FighterRepository.getOne(search);

    if (!item) {
      return null;
    }

    return item;
  }

  getAll() {
    const items = FighterRepository.getAll();

    if (!items) {
      return null;
    }

    return items;
  }

  getOne(id) {
    return this.search({ id });
  }

  create(data) {
    const insensitiveData = this.getInsensitiveData(data);
    const similarFighter = this.getSimilarFighter(insensitiveData);

    if (similarFighter) {
      return null;
    }

    const fighter = FighterRepository.create(data);

    if (!fighter) {
      return null;
    }

    return fighter;
  }

  update(id, dataToUpdate) {
    if (!this.getOne(id)) {
      return null;
    }

    const insensitiveData = this.getInsensitiveData(dataToUpdate);
    const similarFighter = this.getSimilarFighter(insensitiveData);

    if (similarFighter && similarFighter.id !== id) {
      return null;
    }

    const updatedFighter = FighterRepository.update(id, insensitiveData);

    if (!updatedFighter) {
      return null;
    }

    return updatedFighter;
  }

  delete(id) {
    const deletedFighter = FighterRepository.delete(id);

    if (!deletedFighter.length) {
      return null;
    }

    return deletedFighter;
  }
}

module.exports = new FighterService();
