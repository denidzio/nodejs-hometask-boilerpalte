const { UserRepository } = require('../repositories/userRepository');

class UserService {
  getSimilarUsers(data) {
    const similarUser = [];

    let userWithSimilarEmail, userWithSimilarPhoneNumber;

    if (data.email) {
      userWithSimilarEmail = this.search({ email: data.email });
    }

    if (data.phoneNumber) {
      userWithSimilarPhoneNumber = this.search({ phoneNumber: data.phoneNumber });
    }

    if (userWithSimilarEmail) {
      similarUser.push(userWithSimilarEmail);
    }

    if (userWithSimilarPhoneNumber) {
      similarUser.push(userWithSimilarPhoneNumber);
    }

    return similarUser;
  }

  getInsensitiveData(data) {
    const insensitiveData = { ...data };

    if (data.email) {
      insensitiveData.email = data.email.toLowerCase();
    }

    return insensitiveData;
  }

  search(search) {
    const item = UserRepository.getOne(search);

    if (!item) {
      return null;
    }

    return item;
  }

  getAll() {
    const items = UserRepository.getAll();

    if (!items) {
      return null;
    }

    return items;
  }

  getOne(id) {
    return this.search({ id });
  }

  create(data) {
    const insensitiveData = this.getInsensitiveData(data);
    const similarUser = this.getSimilarUsers(insensitiveData);

    if (similarUser.length) {
      return null;
    }

    const user = UserRepository.create(insensitiveData);

    if (!user) {
      return null;
    }

    return user;
  }

  update(id, dataToUpdate) {
    if (!this.getOne(id)) {
      return null;
    }

    const insensitiveData = this.getInsensitiveData(dataToUpdate);
    const similarUsers = this.getSimilarUsers(insensitiveData);

    if (similarUsers.some((user) => user.id !== id)) {
      return null;
    }

    const updatedUser = UserRepository.update(id, insensitiveData);

    if (!updatedUser) {
      return null;
    }

    return updatedUser;
  }

  delete(id) {
    const deletedUser = UserRepository.delete(id);

    if (!deletedUser.length) {
      return null;
    }

    return deletedUser;
  }
}

module.exports = new UserService();
